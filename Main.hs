import System.IO
import Perceptron
import NaiveBayes
import Numeric.Probability.Distribution ( fromFreqs )


main :: IO ()
main = do
  yeswords <- lines `fmap` readFile "yeswords.txt"
  mehwords  <- lines `fmap` readFile "mehwords.txt"
  nowords <- lines `fmap` readFile "nowords.txt"
  testwords <- return ["apertium", "grammaticalframework", "harglebargle", "popopop"]

  let perceptron f word = (mkClassifier yeswords (nowords++mehwords) f) $ f word
     
  putStrLn "Perceptron: unigram"
  putStrLn "-------------------"
  mapM_ pr [(word, perceptron unigramvectors word) | word <- testwords]
  
  putStrLn ""
  putStrLn "Perceptron: bigram"
  putStrLn "------------------"
  mapM_ pr [(word, perceptron bigramvectors word) | word <- testwords]


  let yesMehNo = map (toInteger.length) [yeswords, mehwords, nowords]
      priors = fromFreqs $ zip [Yes,Meh,No] (map fromIntegral yesMehNo)
      bgc = bigramCounts yeswords mehwords nowords
      ugc = unigramCounts yeswords mehwords nowords
  mapM_ print [ (show ug ++ " present in " ++ show st
                , ngramPresentDist st (ug:[]) ugc yesMehNo) 
                | st <- [Yes,Meh,No] , ug <- unigrams ]


pr :: (Show a) => (String, a) -> IO ()
pr (s,a) = putStrLn $ s ++ pad ++ show a
   where pad | length s > 15 = "\t"
             | length s > 7  = "\t\t"
             | otherwise     = "\t\t\t"



