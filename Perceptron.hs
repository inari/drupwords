module Perceptron where

import Learning.Perceptron
import Control.Lens
import Data.Maybe


--------------------------------------------------------------------------------
-- Type synonyms

type FeatureVector = [Int] -- e.g. unigram vector of "abz": [1,1,0,...,1]
type Classifier    = FeatureVector -> Bool


--------------------------------------------------------------------------------
-- Turning strings into feature vectors

unigrams = ['a'..'z']
bigrams  = [[a,b] | a <- unigrams, b <- unigrams]
unigramdict = zip unigrams [0,1..] --[('a',0), ('b',1), ...]
bigramdict  = zip bigrams [0,1..]  --[('aa',0), ('ab',1), ...]

unigramvectors :: String -> FeatureVector
unigramvectors = vectorise unigramdict

bigramvectors :: String -> FeatureVector
bigramvectors = vectorise bigramdict . sublists

sublists :: [a] -> [[a]]
sublists (x:y:[]) = [[x,y]]
sublists (x:y:xs) = [x,y] : sublists (y:xs)
sublists x        = [x]

vectorise :: (Eq a) => [(a,Int)] -> [a] -> FeatureVector
vectorise lookupmap as = go as emptyvector
   where go []     vec = vec
         go (x:xs) vec = let num = fromMaybe 0 $ lookup x lookupmap
                         in  go xs (vec & element num .~ 1)
         emptyvector = take (length lookupmap) $ repeat 0


--------------------------------------------------------------------------------
-- Defining a classifier

mkClassifier :: [String] -> [String] -> (String -> FeatureVector) -> Classifier
mkClassifier pos neg f = Learning.Perceptron.pla trainingdata
   where trainingdata = zip (map f pos) (repeat True) ++ 
                        zip (map f neg) (repeat False)

-- classify :: (String -> FeatureVector) -> String -> Bool
-- classify f word = classify' $ f word 
--    where classify' = mkClassifier yeswords nowords f :: FeatureVector -> Bool


--------------------------------------------------------------------------------
-- TODO: linguistic measures

syllables :: String -> [String]
syllables = undefined


