module NaiveBayes where

import qualified Data.Map as M
import qualified Numeric.Probability.Distribution as PD
import Numeric.Probability.Example.Bayesian
import Perceptron ( unigrams, bigrams, sublists )

-- Different classifiers:
-- * unigram
-- * bigram
-- * number of syllables, other linguistic features TODO

data DrupApproves = Yes | Meh | No
  deriving (Eq,Enum,Ord,Show)


-- Number of yes, nos and mehs containing each unigram/bigram
bigramCounts :: [String] -> [String] -> [String] -> M.Map String [Integer]
bigramCounts yes meh no = M.fromList [ (bg, [cYes, cMeh, cNo]) 
                                            | bg <- bigrams
                                            , let cYes = count bg (concatMap sublists yes)
                                            , let cMeh = count bg (concatMap sublists meh)
                                            , let cNo = count bg (concatMap sublists no) ]

unigramCounts :: [String] -> [String] -> [String] -> M.Map String [Integer]
unigramCounts yes meh no = M.fromList [ (ug:[], [cYes, cMeh, cNo]) 
                                           | ug <- unigrams
                                           , let cYes = count ug (concat yes)
                                           , let cMeh = count ug (concat meh)
                                           , let cNo = count ug (concat no) ]

-- What percentage of each type of word contains a specific ngram.
-- Represent the percentage as a Boolean distribution: "There's a 75% chance of true, and a 25% chance of false."
ngramPresentDist :: DrupApproves -> String -> M.Map String [Integer] -> [Integer] -> Dist Bool
ngramPresentDist approveStatus ngram ngramCountsTable priorCounts =
    boolDist (fromIntegral n / fromIntegral total)
  where ngramCounts = M.findWithDefault [0,0,0] ngram ngramCountsTable
        n     = ngramCounts !! fromEnum approveStatus
        total = priorCounts !! fromEnum approveStatus

boolDist :: Probability -> Dist Bool
boolDist p = PD.fromFreqs [(True, p), (False, 1-p)]

--TODO: update Dist with evidence


mostLikely :: Dist a -> (a, Probability)
mostLikely = head . PD.sortP . PD.decons


--count :: Eq a => a -> [a] -> Integer
count x [] = 0
count x (y:ys) | x==y = 1+(count x ys)
               | otherwise = count x ys
